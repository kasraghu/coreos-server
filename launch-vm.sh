virsh destroy fcos
virsh undefine --remove-all-storage fcos

virt-install --name=fcos --vcpus=2 --ram=2048 --os-variant=fedora-coreos-stable \
    --import --network=bridge=virbr0 \
    --disk size=20 --cdrom coreos-server.iso