* ssh-keygen -t ed25519 -f ./files/keys/user
* `chmod +x generate-*.sh`
* Update Ignition file using [generate-ignition.sh](generate-ignition.sh)
* Create custom ISO file using [generate-iso.sh](generate-iso.sh)
