./generate-ignition.sh

DISK="/dev/disk/by-path/pci-0000:00:12.0-ata-2"
OUTPUT="coreos-server.iso"

while getopts di: flag
do
    case "${flag}" in
        d) DISK="/dev/vda";;

        i) ISO="${OPTARG}";;
    esac
done

coreos-installer download --format iso
rm -I coreos-server.iso

echo "Target disk: ${DISK}"
echo "Input ISO: ${ISO}"

echo "Building ${OUTPUT}"
coreos-installer iso customize \
    --dest-device ${DISK} \
    --dest-ignition coreos-server.ign \
    -o ${OUTPUT} ${ISO}